class nli_sources_updater (
  $sources_updater_code_path = $nli_sources_updater::params::sources_updater_code_path,
  $sources_updater_log_path  = $nli_sources_updater::params::sources_updater_log_path,
  $sources_data_repo         = $nli_sources_updater::params::sources_data_repo,
  $sources_data_base_path    = $nli_sources_updater::params::sources_data_base_path,
  $sources_data_repo_branch  = $nli_sources_updater::params::sources_data_repo_branch,
  $git_repo                  = $nli_sources_updater::params::git_repo,
  $git_branch                = $nli_sources_updater::params::git_branch,
  $bitbucket_login           = $nli_sources_updater::params::bitbucket_login,
  $bitbucket_email           = $nli_sources_updater::params::bitbucket_email,
  $cron_hour                 = $nli_sources_updater::params::cron_hour,
  $cron_min                  = $nli_sources_updater::params::cron_min,
  ) inherits nli_sources_updater::params {


  include git

  git::config { 'user.name':
    value => $bitbucket_login,
    user  => 'puppetadmin',
  }

  git::config { 'user.email':
    value => $bitbucket_email,
    user  => 'puppetadmin',
  }


  ## Vcsrepo for sources updater script

  file { $sources_updater_code_path:
    ensure   => 'directory',
    recurse  => true,
    mode     => "0775",
    owner    => 'puppetadmin',
    group    => 'puppetadmin',
  }

  if $git_repo == undef {
    fail('The git repo must be defined with the variable: git_repo')
  } else {
    vcsrepo { "$sources_updater_code_path":
      ensure   => latest,
      provider => git,
      source   => $git_repo,
      revision => $git_branch,
      owner    => 'puppetadmin',
      group    => 'puppetadmin',
      user     => 'puppetadmin',
      require  => [Package["git"], File["$sources_updater_code_path"]],
    }
  }


  ## Vcsrepo for sources data repository

  file { $sources_data_base_path:
    ensure   => 'directory',
    recurse  => true,
    mode     => "0775",
    owner    => 'puppetadmin',
    group    => 'puppetadmin',
  }

  file { $sources_updater_log_path:
    ensure   => 'directory',
    recurse  => true,
    mode     => "0775",
    owner    => 'puppetadmin',
    group    => 'puppetadmin',
  }

  if $sources_data_repo == undef {
    fail('The sources data repo must be defined with the variable: sources_data_repo')
  } else {
    vcsrepo { "$sources_data_base_path":
      ensure   => latest,
      provider => git,
      source   => "$sources_data_repo.git",
      revision => $sources_data_repo_branch,
      owner    => 'puppetadmin',
      group    => 'puppetadmin',
      user     => 'puppetadmin',
      require  => [Package["git"], File["$sources_data_base_path"], File["$sources_updater_log_path"]],
    }
  }

  ## Package required to run ruby

  package { 'ruby-devel':
    ensure => 'installed',
    install_options => ['--enablerepo=rhel-7-server-optional-rpms'],
  }

  package {'gcc-c++':
    ensure => 'installed',
  }


  ## Ruby Gems

  $required_gems = ['marc', 'rsolr']

  package { $required_gems:
    ensure   => 'installed',
    provider => 'gem',
    require  => [Package['ruby-devel'], Package['gcc-c++']],
  }

  exec {'install-git-gem':
     command => 'gem install git',
     unless  => 'gem list | grep -c git',
     path    => "/usr/bin/",
  }


  ## Template config file for sources updater

  file { "sources-updater-config":
    ensure  => present,
    content => template('nli_sources_updater/config.yaml.erb'),
    path    => "$sources_updater_code_path/config.yaml",
    mode    => '0775',
    require =>  Vcsrepo["$sources_updater_code_path"],
  }


  ## Cron to update sources on a daily basis

  cron {'update-sources-cron':
    command => "ruby $sources_updater_code_path/bin/update_sources.rb > /tmp/sources_updater.log 2>&1",
    user => 'puppetadmin',
    hour => $cron_hour,
    minute => $cron_min,
    environment => 'PATH=usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin',
  }
}