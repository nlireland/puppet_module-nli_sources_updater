class nli_sources_updater::params {
  $sources_updater_code_path = '/usr/local/nli_sources_updater'
  $sources_updater_log_path  = '/usr/local/sources_updater_log_files'
  $sources_data_repo         = 'https://bitbucket.org/nlireland/sources_data'
  $sources_data_base_path    = '/usr/local/sources_data'
  $sources_data_repo_branch  = 'master'
  $git_repo                  = 'https://bitbucket.org/nlireland/sources2marc.git'
  $git_branch                = 'manual-remove-from-solr'
  $bitbucket_login           = "user"
  $bitbucket_email           = "ithelp@nli.ie"
  $cron_hour                 = '21'
  $cron_min                  = '1'
}